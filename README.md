Vagrant LAMP
============

Archivos de configuración de Vagrant para crear un LAMP

Instalación:
-------------

Descarga e instala [VirtualBox](http://www.virtualbox.org/)

Descarga e instala [vagrant](http://vagrantup.com/)

Descarga un box de vagrant (el nombre del box se supone que va a ser precise32)

    $ vagrant box add precise32 http://files.vagrantup.com/precise32.box

Clona este repositorio

Levanta Vagrant

    $ cd [repo]
    $ vagrant up

Qué incluye?:
--------------

Software instalado:

* Apache
* MySQL
* php
* phpMyAdmin
* Xdebug con Webgrind
* zsh con [oh-my-zsh](https://github.com/robbyrussell/oh-my-zsh)
* git, subversion
* mc, vim, screen, tmux, curl
* [MailCatcher](http://mailcatcher.me/)
* [Composer](http://getcomposer.org/)
* Phing
* Herrmientas Drupal:
    * [Drush](http://drupal.org/project/drush)
* Herramientas Wordpress:
    * [WP-Cli](http://wp-cli.org/)
    * [wp2github.py](http://github.com/r8/wp2github.py)
* Herramientas Magento:
    * [n98-magerun](https://github.com/netz98/n98-magerun)
    * [modman](https://github.com/colinmollenhour/modman)
    * [modgit](https://github.com/jreinke/modgit)
* Node.js con los siguientes paquetes:
    * [CoffeeScript](http://coffeescript.org)
    * [Grunt](http://gruntjs.com/)
    * [Bower](http://bower.io)
    * [Yeoman](http://yeoman.io)
    * [LESS](http://lesscss.org)
    * [CSS Lint](http://csslint.net)

Notas
-----

### Apache virtual hosts

Puedes añadir hosts virtuales a apache creando un archivo en `data_bags/sites`.
El directorio raíz del nuevo host virtual será el directorio en `public/`que tenga
el mismo nombre que el `host` que especificaste en el archivo json.
Alternativamente puedes especificar un directorio raíz agregando una clave `docroot`
en el archivo json.

### MySQL

El puerto del cliente es 3306 y está disponible en el huésped en el 33066. También está disponible en todos los dominios. 
Usuario: root
Clave: vagrant

### phpMyAdmin

phpMyAdmin disponible en todos los dominios. Por ejemplo:

    http://local.dev/phpmyadmin

### XDebug y webgrind

XDebug está configurado para conectacse a tu máquina huésped en el puerto 9000 cuando
se empieza una sesión de debug desde un navegador corriendo en tu huésped. 
Una sesión de debug siempre se empieza añadiendo la variable GET XDEBUG_SESSION_START a la URL.

XDebug también está configurado para generar salida de perfil cachegrind por demanda
mediante la variable GET XDEBUG_PROFILE en la URL. Por ejemplo:

    http://local.dev/index.php?XDEBUG_PROFILE

Webgrind está disponible en cada dominio. Por ejemplo:

    http://local.dev/webgrind

Busca archivos cachegrind en el direcotiro `/tmp`, donde los deja xdebug.

**Nota:** xdebug usa el valor por defecto para xdebug.profiles_output_name, 
lo cual significa que el archivo de salido solo incluye el ID de proceso como
parte única. Si quieres configurar xdebug para que siempre cree salido profiler 
(`xdebug.profiles_enable = 1`), necesitarás cambiar este ajusto a algo como
 
    xdebug.profiler_output_name = cachegrind.out.%t.%p
 
asi que tu llamada a webgrind no sobreescribirá el archivo para el proceso que
está sirviendo webgrind.    

### Mailcatcher

Todos los emails enviados por PHP son interceptados por MailCatcher. Así que normalmente ningún email será entregado fuera de la máquina virtual. En vez de eso puedes comprobar los mensajes empleando el frontend de MailCatcher, el cual
corre en el puerto 1080 y está disponible para todos los domimios:

    http://local.dev:1080

### Composer

El binario de Composer está instalado globalmente (a `/usr/local/bin`), asi que simplemente puedes llamarlo mediante `composer` en cualquier directorio.
